import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular1'
    
  name = 'Mohamad Reza'
  age = 23
  status = true

  person = {
    title: 'Test A',
    name: 'rezazzz',
    age: 0,
    status: true
  }
  personList = [
  {
    title: 'Test X',
    name: 'rezazzz',
    age: 0,
    status: true
  },
  {
    title: 'Test Y',
    name: 'REZAZ',
    age: 0,
    status: true
  }]

  constructor() {
    this.name = 'Reza Contruct'
    this.age = 20
    this.status = true
  }
  onCallBack (ev: any) {
    console.log(ev);
  }
  dataArray = new Array (10)
  datas = [1,2]

  onCallBackAdd(ev: any){
    ev.data.title = "Ini tambahan aja sih"
    this.personList.push(ev.data)
    console.log(this.personList)
  }
}
