import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { TransactionList } from '../interfaces/transaction.interface';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  public readonly baseApi = "http://localhost:8080"

  constructor(private httpClient: HttpClient) { }

  getTransactionList(): Observable<TransactionList[]> {
    
    return this.httpClient.get<ResponseBase<TransactionList[]>>(this.baseApi + "/api/transaksiFP").pipe(
      map(val => {
        console.log(val)
        const data: TransactionList[] = [];
        for (let obj of val.data) {
          data.push ( {
            id: obj.id,
            tanggalTransaksi: obj.tanggalTransaksi,
            totalHarga: obj.totalHarga,
            kuantitas: obj.kuantitas,
            product: {
              namaMaterial: obj.product.namaMaterial,
              hargaSatuan: obj.product.hargaSatuan,
            },
            user: {
              username: obj.user.username,
              fullname: obj.user.fullname,
              noHandphone: obj.user.noHandphone,
              alamat: obj.user.alamat,
            },
            payment: obj.payment,
            paid: obj.paid,
          })
        }
        return data
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }
}

