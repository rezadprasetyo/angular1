import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ProductList } from '../interfaces/product.interface';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public readonly baseApi = "http://localhost:8080"

  constructor(private httpClient: HttpClient) { }

  getProductList(): Observable<ProductList[]> {
    
    return this.httpClient.get<ResponseBase<ProductList[]>>(this.baseApi + "/api/productFP").pipe(
      map(val => {
        console.log(val)
        const data: ProductList[] = [];
        for (let obj of val.data) {
          data.push ( {
            id: obj.id,
            namaMaterial: obj.namaMaterial,
            stocks: obj.stocks,
            hargaSatuan: obj.hargaSatuan,
            satuan: obj.satuan,
            photo: `${this.baseApi}/api/files/${obj.photo}:.+`,
          })
        }
        return data
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  product(payload: {namaMaterial: String, hargaSatuan: String, stocks: String, photo?: String}) {
    return this.httpClient.post(this.baseApi + '/api/productFP', payload) 
  }
  

  uploadPhotoProduct(data: File): Observable<ResponseBase<string>> {
    const file = new FormData();
    file.append('file', data, data.name);
    return this.httpClient.post<ResponseBase<string>>(
      `${this.baseApi}/api/upload-file`,
      file
    );
  }

  editProduct(
    payload: {
      namaMaterial: string;
      hargaSatuan: number;
      stocks: number;
      photo?: string;
    },
    id: number
  ) {
    return this.httpClient.put(
      this.baseApi + '/api/productFP/' + id,
      payload
    );
  }
  deleteProduct(
    payload: { namaMaterial: string;
      hargaSatuan: number;
      stocks: number;
      photo?: string; 
    },
    id: number
  ) {
    return this.httpClient.delete(
      this.baseApi + '/api/productFP/' + id);
  }
}

