import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequestLogin, ResponseLogin, } from '../interfaces/auth.interface';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseApi = "http://localhost:8080"

  public isAdmin = new BehaviorSubject<Boolean>(false)
  public isUser = new BehaviorSubject<Boolean>(false)

  public isAuthenticated = new BehaviorSubject<boolean>(false)

  constructor(private httpClient: HttpClient) {
    this.isLogin()
   }

login(payload: RequestLogin): Observable<ResponseBase<ResponseLogin>> {
  return this.httpClient.post<ResponseBase<ResponseLogin>>(this.baseApi + '/login', payload) .pipe(
    tap( val => {
      console.log(val)
      if (val.data.jwtrole === 'ROLE_ADMIN') {
        this.isAdmin.next(true)
      } else if (val.data.jwtrole === 'ROLE_USER') {
        this.isUser.next(true)
      }
    })
  )
  }

register(payload: {username: String, password: String, noHandphone: String, fullname : String, alamat: String, dob: String, photo?: String}) {
  return this.httpClient.post(this.baseApi + '/register', payload) 
}

isLogin() {
  const token = localStorage.getItem('token')
  if (token) {
    this.isAdmin.next(true)
  }
}
}
