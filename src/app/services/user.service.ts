import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { UserList } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public readonly baseApi = "http://localhost:8080"
  
  constructor(private httpClient: HttpClient) { }

  getAPUsers(): Observable<UserList[]> {

    return this.httpClient.get<ResponseBase<UserList[]>>(this.baseApi + "/api/getAllUsers").pipe(
      map(val => {
        console.log(val)
        const data: UserList[] = [];
        for (let item of val.data) {
          data.push ( {
            id: item.id,
            username: item.username,
            fullname: item.fullname,
            alamat: item.alamat,
            dob: item.dob,
            noHandphone: item.noHandphone,
            role: item.role,
            
          })
        }
        return data
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  getCurrentUser(): Observable<UserList> {

    return this.httpClient.get<ResponseBase<UserList>>(`${this.baseApi}/api/getCurrentUser`).pipe(
      map((val) => {
        return val.data; 
        }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  uploadPhoto(data: File): Observable<ResponseBase<string>> {
    const file = new FormData();
    file.append('file', data, data.name);
    return this.httpClient.post<ResponseBase<string>>(
      `${this.baseApi}/api/upload-file`,
      file
    );
  }
}

