import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseBase } from 'src/app/interfaces/response.interface';
import { UserList } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  currentProfile$: Observable<UserList> = this.userService.getCurrentUser()

  constructor(private userService: UserService) {}

}
