import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  constructor(private router: Router) {}

  doClickLogout() {
    Swal.fire({
      icon: "warning",
      title: "Are you sure you want to logout?",
      showCancelButton: true,
      cancelButtonText: `Back`,
      showDenyButton: true,
      denyButtonText: `Yes, I'm sure.`,
      showConfirmButton: false,
    }).then((result) => {
      if (result.isDenied) {
        Swal.fire({
          title: "You have succesfully logout",
          icon: "success",
          confirmButtonColor: "#0046e6",
        });

        this.router.navigate(["/login"]);
      }
      localStorage.clear();
    });
  }

}
