import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';

const routes: Routes = [
{
  path: 'login',
  component: LoginComponent  
},
{
  path: 'register',
  component: RegisterComponent
},
{
  path: 'admin',
  loadChildren: () =>
    import('./pages/admin/admin.module').then((m) => m.AdminModule),
  // canLoad: [AdminGuard],
},
{
  path: '',
  redirectTo: 'login',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
