import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  
  @Input() title = 'Angular1'
  @Input() name = 'Mohamad Reza'
  @Input() age = 23
  @Input() status = true

  @Output() dataCallBack = new EventEmitter()

  doClick() {
    this.dataCallBack.emit({ data: { name: this.name, age: this.age, status: this.status }})
  }
  
}
