import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpInterceptorCore } from './core/http.interceptor';
import { registerLocaleData } from '@angular/common';
import localId from '@angular/common/locales/id'

registerLocaleData(localId, 'id')

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  providers: [ 
    { provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorCore,
      multi: true
    },
    {
      provide: LOCALE_ID,
      useValue: 'id'
    }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
