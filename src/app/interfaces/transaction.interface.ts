export interface TransactionList {
    id: number,
    tanggalTransaksi: string,
    totalHarga: number,
    kuantitas: number,
    product: {
        namaMaterial: string,
        hargaSatuan: number,
    }
    user: {
        username: string,
        fullname: string,
        noHandphone: number
        alamat: string,
    }
    payment: {
        tanggalPembayaran: string,
        waktuPembayaran: string,
    } |null
    paid: boolean,
}

