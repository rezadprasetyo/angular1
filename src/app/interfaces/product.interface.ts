export interface ProductList {
    id: number,
    namaMaterial: string,
    hargaSatuan: number,
    stocks: number,
    satuan: string,
    photo?: string
  }
  export interface ResponseUploadPhoto {
    image: string
  }