export interface ResponseBase<T> {
    message: string;
    success: boolean;
    data: T
}