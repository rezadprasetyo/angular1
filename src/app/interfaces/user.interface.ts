export interface UserList {
    id: number,
    username: string,
    fullname: string,
    alamat: string,
    noHandphone: string,
    dob: string,
    role: string
  }
export interface ResponseUploadPhoto {
    image: string
  }