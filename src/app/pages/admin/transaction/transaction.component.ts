import { Component, TemplateRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject, takeUntil } from 'rxjs';
import { TransactionList } from 'src/app/interfaces/transaction.interface';
import { AuthService } from 'src/app/services/auth.service';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent {
  modalRef?: BsModalRef;

  transaction: TransactionList[] = [];
  refresh = new Subject<void>();

  constructor(private transactionService: TransactionService, 
    private modalService: BsModalService, 
    private authService: AuthService, private formBuilder: FormBuilder) {
    this.loadData();
    this.transactionService.getTransactionList().subscribe(
    response => {
      console.log(response)
      this.transaction = response
    });
  }
  loadData() {
    this.transactionService
      .getTransactionList()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.transaction = response;
      });
  }
  transactionDetail: TransactionList = {
    id: 0,
    tanggalTransaksi: '',
    totalHarga: 0,
    kuantitas: 0,
    product: {
      namaMaterial: '',
      hargaSatuan: 0,
    },
    user: {
      username: '',
      fullname: '',
      noHandphone: 0,
      alamat: '',
    },
    payment: {
      tanggalPembayaran: '',
      waktuPembayaran: ''
    },
    paid: false,
  };

  getDetail(detail: TransactionList) {
    console.log(detail);
    this.transactionDetail = detail;
  }
  openModalDetail(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
