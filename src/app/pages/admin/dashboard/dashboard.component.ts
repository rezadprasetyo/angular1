import { Component, OnInit, TemplateRef } from '@angular/core';
import { UserList } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder} from '@angular/forms'; 
import { ProductList } from 'src/app/interfaces/product.interface';
import { ProductService } from 'src/app/services/product.service';
import { TransactionList } from 'src/app/interfaces/transaction.interface';
import { TransactionService } from 'src/app/services/transaction.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent{

  allUsers: UserList[] = []
  allProducts: ProductList[] = []
  allTransactions: TransactionList[] = []


  constructor(private userService: UserService, private productService: ProductService, private transactionService: TransactionService) {
    this.userService.getAPUsers().pipe().subscribe((response) => {
      console.log(response);
      this.allUsers = response;
    });
    this.productService.getProductList().pipe().subscribe(
      (response) => {
        console.log(response);
        this.allProducts = response;
      });
    this.transactionService.getTransactionList().pipe().subscribe(
      (response) => {
        console.log(response);
        this.allTransactions = response
      });
    }


  }
  
  // loadData() {
  //   this.userService
  //     .getAPUsers()
  //     .pipe(takeUntil(this.refresh))
  //     .subscribe((response) => {
  //       console.log(response);
  //       this.user = response;
  //     });

  // }

