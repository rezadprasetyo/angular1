import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductComponent } from './product/product.component';
import { UsersComponent } from './users-list/users.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransactionComponent } from './transaction/transaction.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'productFP',
    component: ProductComponent
  },
  {
    path: 'getAllUsers',
    component: UsersComponent
  },
  {
    path: 'transaksiFP',
    component: TransactionComponent
  },
  {
    path: 'editUserFP',
    component: ProfileComponent
  }

]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, ProductComponent, ProductComponent, UsersComponent, TransactionComponent, ProfileComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AdminModule { }
