import { Component, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { ProductList } from 'src/app/interfaces/product.interface';
import { AuthService } from 'src/app/services/auth.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {

  modalRef?: BsModalRef;
  product: ProductList[] = [];

  photo!: string
  photoFile!: File|null

  formAddProduct: FormGroup;
  formEditProduct: FormGroup;
  refresh = new Subject<void>();

  productDetail: ProductList = {
    id: 0,
    namaMaterial: '',
    hargaSatuan: 0,
    stocks: 0,
    satuan: '',
    photo: '',
  };

  constructor(private productService: ProductService, private modalService: BsModalService, private authService: AuthService,
    private formBuilder: FormBuilder) {

      this.formAddProduct = this.formBuilder.group({
        namaMaterial: ['', [Validators.required, ]],
        hargaSatuan: ['', [Validators.required, ]],
        stocks: ['', [Validators.required]],
        satuan: ['', Validators.required],
        photo: ['', [Validators.required]],
      });

      this.formEditProduct = this.formBuilder.group({
        namaMaterial: [''],
        hargaSatuan: [''],
        stocks: [''],
        satuan: [''],
        photo: [''],
      });

      this.productService.getProductList().subscribe(
        response => {
          console.log(response)
          this.product = response
        });  
        
     }
     loadData() {
      this.productService.getProductList().pipe(takeUntil(this.refresh))
        .subscribe((response) => {
          console.log("refresh ya" + response);
          this.product = response;
        });
    }
  
     get errorControl() {
      return this.formAddProduct.controls;
    }

    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
    }
    showPreview(event: any){
      if(event){
        const file = event.target.files[0]
        this.photoFile = file
        const reader = new FileReader();
        reader.onload = () => {
          this.photo = reader.result as string
        }
        reader.readAsDataURL(file);
      }
    }
  
    doAddProduct() {
      if (this.photoFile) {
        this.productService.uploadPhotoProduct(this.photoFile).pipe(
          switchMap(val => {
    
            const payload = {
              namaMaterial: this.formAddProduct.value.namaMaterial,
              hargaSatuan: this.formAddProduct.value.hargaSatuan,
              stocks: this.formAddProduct.value.stocks,
              satuan: this.formAddProduct.value.satuan,
              photo: val.data
            }
    
            return this.productService.product(payload).pipe(
              map(val => val)
            )
          })
        ).subscribe(response => {
           console.log(response)
           Swal.fire(
            'Data berhasil terinput!',
            'Silahkan cek kembali di halaman ini',
            'success'
          )
          this.loadData();
           })
          }
      }
      
    doAction(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
      }
    
    doEditModal(data: ProductList) {
      console.log(data);
      this.productDetail = data;
      this.formEditProduct.controls['namaMaterial'].patchValue(data.namaMaterial);
      this.formEditProduct.controls['hargaSatuan'].patchValue(data.hargaSatuan);
      this.formEditProduct.controls['stocks'].patchValue(data.stocks);
      this.formEditProduct.controls['satuan'].patchValue(data.satuan);
      this.formEditProduct.controls['photo'].patchValue(data.photo);
    }
    doEditProduct() {
      if (this.photoFile) {
        this.productService.uploadPhotoProduct(this.photoFile).pipe(
          switchMap(val => {
    
            const payload = {
              namaMaterial: this.formEditProduct.value.namaMaterial,
              hargaSatuan: this.formEditProduct.value.hargaSatuan,
              stocks: this.formEditProduct.value.stocks,
              satuan: this.formEditProduct.value.satuan,
              photo: val.data
            }
            this.photoFile = null
            return this.productService.editProduct(payload, this.productDetail.id).pipe(
              map(val => val)
            )
          })
        ).subscribe(response => {
           console.log(response)
           Swal.fire(
            'Data berhasil diubah!',
            'Silahkan cek kembali perubahan datanya',
            'success'
          )
          this.loadData();
           })
      } else {
        const payload = {
          namaMaterial: this.formEditProduct.value.namaMaterial,
          hargaSatuan: this.formEditProduct.value.hargaSatuan,
          stocks: this.formEditProduct.value.stocks,
          satuan: this.formEditProduct.value.satuan,
          photo: 
          this.formEditProduct.value.photo ??
          this.productDetail.photo,
        };
        this.productService
          .editProduct(payload, this.productDetail.id)
          .pipe(map((val) => val))
          .subscribe((response) => {
            console.log(response);
            this.loadData();
          });
      }
    }
  
    // getProductDetail(detail: any) {
    //   console.log(detail);
    //   this.productDetail = detail;
  
    //   this.formEditProduct.controls['id'].patchValue(detail.id);
    //   this.formEditProduct.controls['judulKategori'].patchValue(
    //     detail.judulKategori
    //   );
    //   //debugger
    //   //this.formEditCategory.controls['iconKategori'].patchValue(detail.iconKategori)
    // }

    doDeleteProduct() {
      if (this.photoFile) {
        this.productService.uploadPhotoProduct(this.photoFile).pipe(
          switchMap(val => {
    
            const payload = {
              namaMaterial: this.formEditProduct.value.namaMaterial,
              hargaSatuan: this.formEditProduct.value.hargaSatuan,
              stocks: this.formEditProduct.value.stocks,
              satuan: this.formEditProduct.value.satuan,
              photo: val.data
            }
            this.photoFile = null
            return this.productService.deleteProduct(payload, this.productDetail.id).pipe(
              map(val => val)
            )
          })
          )
          .subscribe((response) => {
            console.log(response);
            Swal.fire(
            'Data yang kamu pilih berhasil terhapus!',
            'Silahkan cek kembali',
            'success'
            );
            this.loadData();
          });
      } else {
        const payload = {
          namaMaterial: this.formEditProduct.value.namaMaterial,
          hargaSatuan: this.formEditProduct.value.hargaSatuan,
          stocks: this.formEditProduct.value.stocks,
          satuan: this.formEditProduct.value.satuan,
          photo: this.productDetail.photo
        };
        this.productService
          .deleteProduct(payload, this.productDetail.id)
          .pipe(map((val) => val))
  
          .subscribe((response) => {
            console.log(response);
            Swal.fire(
              'Data yang kamu pilih berhasil terhapus!',
              'Silahkan cek kembali',
              'success'
              );
            this.loadData();
          });
      }
    }
  
    getDeleteModal(data: any) {
      console.log(data);
      this.productDetail = data;
    }
  

  }