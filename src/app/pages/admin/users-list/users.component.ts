import { Component} from '@angular/core';
import { UserList } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {

  user: UserList[] = []

  constructor(private userService: UserService) {

      this.userService.getAPUsers().subscribe(
      response => {
        console.log(response)
        this.user = response
      });
    }

}
