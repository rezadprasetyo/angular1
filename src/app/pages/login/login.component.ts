import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  username = ""
  password = ""

  formLogin: FormGroup

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder){
      this.formLogin = this.formBuilder.group({
        username: ['', [Validators.required, Validators.minLength(4)]],
        password: ['', [Validators.required, Validators.minLength(8)]]
      })
    }
    get errorControl(){
      return this.formLogin.controls;
    }

    doLogin() {

      console.log(this.formLogin)
      const payload = {
        username: this.formLogin.value.username,
        password: this.formLogin.value.password,
      }

      this.authService.login(payload).subscribe(
        (response) => {
          console.log(response)
          Swal.fire(
            'Login Sukses!',
            'Akunmu berhasil masuk website',
            'success'
          )
        // alert(response.data.jwtrole)
        if(response.data.jwtrole === 'ROLE_ADMIN'){
          localStorage.setItem('jwttoken', response.data.jwttoken)
          this.router.navigate(['/admin/dashboard'])

          // 'User
        } else if(response.data.jwtrole === 'ROLE_USER') {
          Swal.fire({
            title: 'Login Gagal!',
            text: 'Hanya admin yang dapat mengakses halaman ini',
            icon: 'error',
            confirmButtonText: 'Try Again'
          })
        } else {
          this.router.navigate(['/playground'])
        }
          },
          error => {    
            console.log(error);
            Swal.fire({
              title: 'Error! Login Gagal!',
              text: 'Username atau password salah!',
              icon: 'error',
              confirmButtonText: 'Try Again'
            })
            // alert(error.error.message)
          }
        )
      }
    }

