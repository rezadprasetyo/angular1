import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent {

  formRegister: FormGroup;

  username = ""
  password = ""
  checkPassword = ""
  noHandphone = ""
  fullname = ""
  alamat = ""
  dob = ""

  constructor(private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder){
      this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      checkPassword: ['', [Validators.required, Validators.minLength(8)]],
      noHandphone: ['', [Validators.required,]],
      fullname: ['', [Validators.required,]],
      alamat: ['', [Validators.required,]],
      dob: ['', [Validators.required,]]
    })
  }

  get errorControl(){
    return this.formRegister.controls;
  }

  get confirmPassword() {
    return this.formRegister.value.password == this.formRegister.value.checkPassword
  }

  doRegister() {
    console.log(this.formRegister)
      const payload = {
        username: this.formRegister.value.username,
        password: this.formRegister.value.password,
        checkPassword: this.formRegister.value.checkPassword,
        noHandphone: this.formRegister.value.noHandphone,
        fullname: this.formRegister.value.fullname,
        alamat: this.formRegister.value.alamat,
        dob: this.formRegister.value.dob
      }

    this.authService.register(payload).subscribe(
      (response) => {
        console.log(response)
        Swal.fire(
          'Register Sukses!',
          'Akunmu berhasil terdaftar dalam sistem',
          'success'
        )
        }
      )
    }  
}
